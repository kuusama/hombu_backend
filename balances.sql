BEGIN
 DECLARE done TINYINT DEFAULT FALSE;
 DECLARE v_date DATE;
 DECLARE v_balance FLOAT;
 DECLARE curs1 CURSOR FOR SELECT date1, balance FROM `tmptable`;
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DROP TABLE IF EXISTS `tmptable`; 
CREATE TABLE `tmptable` (date1 DATE, balance FLOAT) ENGINE = MEMORY;

set @currDate = dateFrom;

WHILE @currDate < dateTo DO

set @strTimestamp := UNIX_TIMESTAMP(@currDate);
set @balanceQuery =  CONCAT('(SELECT SUM(`transaction`.`amount`) FROM `transaction` WHERE `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) <= ', @strTimestamp ,' )');

set @query2 = concat('insert into tmptable(date1, balance) values (''', DATE_FORMAT(@currDate,'%Y-%m-%d'), ''', ' ,  @balanceQuery , ' );');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

    IF intervalType = 1 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 DAY);
    ELSEIF intervalType = 2 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 WEEK);
    ELSEIF intervalType = 3 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 MONTH);
    ELSEIF intervalType = 4 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 YEAR);
    END IF;

END WHILE;

set @strTimestamp := UNIX_TIMESTAMP(dateTo);
set @balanceQuery =  CONCAT('(SELECT SUM(`transaction`.`amount`) FROM `transaction` WHERE `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) <= ', @strTimestamp ,' )');
set @query2 = concat('insert into tmptable(date1, balance) values (''', DATE_FORMAT(@currDate,'%Y-%m-%d'), ''', ' ,  @balanceQuery , ' );');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

SET @json = '{"data":[';

OPEN curs1;

my_loop:
 LOOP
  FETCH FROM curs1 INTO v_date, v_balance;

  IF done THEN 
    LEAVE my_loop;
  ELSE
    SET @json = CONCAT(@json, '{"date": "', v_date, '", "balance": "', v_balance, '"},');
  END IF;
 END LOOP;
 
SET jSON = CONCAT(TRIM(TRAILING ',' FROM @json), ']}');

SELECT jSON;

DROP TABLE IF EXISTS `tmptable`;

END