BEGIN
 DECLARE done TINYINT DEFAULT FALSE;
 DECLARE v_date DATE;
 DECLARE v_account VARCHAR(255);
 DECLARE v_balance FLOAT;
 DECLARE curs1 CURSOR FOR SELECT date1, account, balance FROM `tmptable` ORDER BY account, date1;
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DROP TABLE IF EXISTS `tmptable`; 
CREATE TABLE `tmptable` (date1 DATE, account VARCHAR(255), balance FLOAT) ENGINE = MEMORY;

set @currDate = dateFrom;

WHILE @currDate < dateTo DO

set @strTimestamp := UNIX_TIMESTAMP(@currDate);
set @balanceQuery =  CONCAT('(SELECT ''', DATE_FORMAT(@currDate, '%Y-%m-%d'), ''' as dat, `account`.`name`, SUM(`transaction`.`amount`) FROM `transaction` inner join `account` on `account`.`id` = `transaction`.`account` WHERE `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) <= ', @strTimestamp ,' GROUP BY `transaction`.`account`)');

set @query2 = concat('insert into tmptable(date1, account, balance)  ', @balanceQuery , ' ;');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

    IF intervalType = 1 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 DAY);
    ELSEIF intervalType = 2 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 WEEK);
    ELSEIF intervalType = 3 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 MONTH);
    ELSEIF intervalType = 4 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 YEAR);
    END IF;

END WHILE;

set @strTimestamp := UNIX_TIMESTAMP(dateTo);
set @balanceQuery =  CONCAT('(SELECT ''', DATE_FORMAT(@currDate, '%Y-%m-%d'), ''' as dat, `account`.`name`, SUM(`transaction`.`amount`) FROM `transaction`
inner join `account` on `account`.`id` = `transaction`.`account` WHERE `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) <= ', @strTimestamp ,' GROUP BY `transaction`.`account`)');
set @query2 = concat('insert into tmptable(date1, account, balance) ', @balanceQuery , ' ;');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

SET @json = '{"data":[';

OPEN curs1;

my_loop:
 LOOP
  FETCH FROM curs1 INTO v_date, v_account, v_balance;

  IF done THEN 
    LEAVE my_loop;
  ELSE
    SET @json = CONCAT(@json, '{"date": "', v_date, '", "account": "', v_account, '", "balance": "', v_balance, '"},');
  END IF;
 END LOOP;
 
SET jSON = CONCAT(TRIM(TRAILING ',' FROM @json), ']}');

SELECT jSON;

DROP TABLE IF EXISTS `tmptable`;

END