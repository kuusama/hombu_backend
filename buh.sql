-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Мар 29 2018 г., 13:18
-- Версия сервера: 10.1.29-MariaDB
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `buh`
--
CREATE DATABASE IF NOT EXISTS `buh` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `buh`;

DELIMITER $$
--
-- Процедуры
--
DROP PROCEDURE IF EXISTS `fillTmpTable`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `fillTmpTable` (IN `jS` TEXT)  MODIFIES SQL DATA
BEGIN
 DECLARE done TINYINT DEFAULT FALSE;
 DECLARE v_id INT(11) default 0;
 DECLARE m VARCHAR(20);
 DECLARE curs1 CURSOR FOR SELECT id, mmm FROM `tmptable`;
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DROP TABLE IF EXISTS `tmptable`;
CREATE TEMPORARY TABLE `tmptable` (id INT, mmm ChAR(30), INDEX USING HASH (id)) ENGINE = MEMORY;

set @count = 1;

WHILE @count < 11 DO

set @query2 = concat('insert into tmptable(id, mmm) values (', @count, ' , ''222'')');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

SET @count := @count + 1;

END WHILE;

SET @json = '{"data":[';

OPEN curs1;

my_loop: -- loops have to have an arbitrary label; it's used to leave the loop
 LOOP
  FETCH FROM curs1 INTO v_id, m;

  IF done THEN 
    LEAVE my_loop; 
  ELSE 
    SET @json = CONCAT(@json, '{"id":', v_id, ', "mmm":', m, '},');
  END IF;
 END LOOP;
 
SET @json = CONCAT(TRIM(TRAILING ',' FROM @json), ']}');

SET @jS = @json;
SELECT @jS;

DROP TABLE IF EXISTS `tmptable`;

END$$

DROP PROCEDURE IF EXISTS `getBalances`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getBalances` (IN `userid` INT, IN `dateFrom` DATE, IN `dateTo` DATE, IN `intervalType` TINYINT, OUT `jSON` TEXT)  MODIFIES SQL DATA
    COMMENT 'Get balances for user for all accounts'
BEGIN
 DECLARE done TINYINT DEFAULT FALSE;
 DECLARE v_date DATE;
 DECLARE v_balance FLOAT;
 DECLARE curs1 CURSOR FOR SELECT date1, balance FROM `tmptable`;
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DROP TABLE IF EXISTS `tmptable`; 
CREATE TABLE `tmptable` (date1 DATE, balance FLOAT) ENGINE = MEMORY;

set @currDate = dateFrom;

WHILE @currDate < dateTo DO

set @strTimestamp := UNIX_TIMESTAMP(@currDate);
set @balanceQuery =  CONCAT('(SELECT SUM(`transaction`.`amount`) FROM `transaction` WHERE `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) <= ', @strTimestamp ,' )');

set @query2 = concat('insert into tmptable(date1, balance) values (''', DATE_FORMAT(@currDate,'%Y-%m-%d'), ''', ' ,  @balanceQuery , ' );');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

    IF intervalType = 1 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 DAY);
    ELSEIF intervalType = 2 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 WEEK);
    ELSEIF intervalType = 3 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 MONTH);
    ELSEIF intervalType = 4 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 YEAR);
    END IF;

END WHILE;

set @strTimestamp := UNIX_TIMESTAMP(dateTo);
set @balanceQuery =  CONCAT('(SELECT SUM(`transaction`.`amount`) FROM `transaction` WHERE `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) <= ', @strTimestamp ,' )');
set @query2 = concat('insert into tmptable(date1, balance) values (''', DATE_FORMAT(@currDate,'%Y-%m-%d'), ''', ' ,  @balanceQuery , ' );');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

SET @json = '{"data":[';

OPEN curs1;

my_loop:
 LOOP
  FETCH FROM curs1 INTO v_date, v_balance;

  IF done THEN 
    LEAVE my_loop;
  ELSE
    SET @json = CONCAT(@json, '{"date": "', v_date, '", "balance": "', v_balance, '"},');
  END IF;
 END LOOP;
 
SET jSON = CONCAT(TRIM(TRAILING ',' FROM @json), ']}');

SELECT jSON;

DROP TABLE IF EXISTS `tmptable`;

END$$

DROP PROCEDURE IF EXISTS `getBalancesDetail`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getBalancesDetail` (IN `userid` INT, IN `dateFrom` DATE, IN `dateTo` DATE, IN `intervalType` INT, OUT `jSON` TEXT)  NO SQL
BEGIN
 DECLARE done TINYINT DEFAULT FALSE;
 DECLARE v_date DATE;
 DECLARE v_account VARCHAR(255);
 DECLARE v_balance FLOAT;
 DECLARE curs1 CURSOR FOR SELECT date1, account, balance FROM `tmptable` ORDER BY account DESC, date1;
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DROP TABLE IF EXISTS `tmptable`; 
CREATE TABLE `tmptable` (date1 DATE, account VARCHAR(255), balance FLOAT) ENGINE = MEMORY;

set @currDate = dateFrom;

WHILE @currDate < dateTo DO

set @strTimestamp := UNIX_TIMESTAMP(@currDate);
set @balanceQuery =  CONCAT('(SELECT ''', DATE_FORMAT(@currDate, '%Y-%m-%d'), ''' as dat, `account`.`name`, SUM(`transaction`.`amount`) FROM `transaction` inner join `account` on `account`.`id` = `transaction`.`account` WHERE `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) <= ', @strTimestamp ,' GROUP BY `transaction`.`account`)');

set @query2 = concat('insert into tmptable(date1, account, balance)  ', @balanceQuery , ' ;');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

    IF intervalType = 1 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 DAY);
    ELSEIF intervalType = 2 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 WEEK);
    ELSEIF intervalType = 3 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 MONTH);
    ELSEIF intervalType = 4 THEN
        SET @currDate := DATE_ADD(@currDate, INTERVAL 1 YEAR);
    END IF;

END WHILE;

set @strTimestamp := UNIX_TIMESTAMP(dateTo);
set @balanceQuery =  CONCAT('(SELECT ''', DATE_FORMAT(@currDate, '%Y-%m-%d'), ''' as dat, `account`.`name`, SUM(`transaction`.`amount`) FROM `transaction`\r\ninner join `account` on `account`.`id` = `transaction`.`account` WHERE `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) <= ', @strTimestamp ,' GROUP BY `transaction`.`account`)');
set @query2 = concat('insert into tmptable(date1, account, balance) ', @balanceQuery , ' ;');
PREPARE tmpQ from @query2;
EXECUTE tmpQ;

SET @json = '{"data":[';

OPEN curs1;

my_loop:
 LOOP
  FETCH FROM curs1 INTO v_date, v_account, v_balance;

  IF done THEN 
    LEAVE my_loop;
  ELSE
    SET @json = CONCAT(@json, '{"date": "', v_date, '", "account": "', v_account, '", "balance": "', v_balance, '"},');
  END IF;
 END LOOP;
 
SET jSON = CONCAT(TRIM(TRAILING ',' FROM @json), ']}');

SELECT jSON;

DROP TABLE IF EXISTS `tmptable`;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `account`
--

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `currency` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `account`
--

INSERT INTO `account` (`id`, `user`, `name`, `currency`, `icon`) VALUES
(1, 1, 'Кошелёк', 53, '82'),
(5, 1, 'Заначка', 53, '83'),
(8, 3, 'Main account', 1, '1'),
(9, 3, 'Second account', 1, '1');

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `otype` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `user`, `name`, `otype`, `icon`) VALUES
(1, 1, 'На водку', 2, '124'),
(2, 1, 'На пиво', 2, '125'),
(10, 1, 'На траву', 2, '127'),
(12, 1, 'Зарплата', 1, '78'),
(13, 1, 'Продажа в секонд-хэнд', 1, '79'),
(16, 3, 'Sample income category', 1, '1'),
(17, 3, 'Sample expense category', 2, '2');

-- --------------------------------------------------------

--
-- Структура таблицы `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `abr` varchar(5) DEFAULT NULL,
  `sign` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `currency`
--

INSERT INTO `currency` (`id`, `name`, `abr`, `sign`) VALUES
(1, 'Рубль', NULL, 'р.'),
(2, 'Иена', NULL, 'Y'),
(3, 'Albania Lek', 'ALL', 'Lek'),
(4, 'Afghanistan Afghani', 'AFN', '؋'),
(5, 'Argentina Peso', 'ARS', '$'),
(6, 'Aruba Guilder', 'AWG', 'ƒ'),
(7, 'Australia Dollar', 'AUD', '$'),
(8, 'Azerbaijan Manat', 'AZN', '₼'),
(9, 'Bahamas Dollar', 'BSD', '$'),
(10, 'Barbados Dollar', 'BBD', '$'),
(11, 'Belarus Ruble', 'BYN', 'Br'),
(12, 'Belize Dollar', 'BZD', 'BZ$'),
(13, 'Bermuda Dollar', 'BMD', '$'),
(14, 'Bolivia Bolíviano', 'BOB', '$b'),
(15, 'Bosnia and Herzegovina Convertible Marka', 'BAM', 'KM'),
(16, 'Botswana Pula', 'BWP', 'P'),
(17, 'Bulgaria Lev', 'BGN', 'лв'),
(18, 'Brazil Real', 'BRL', 'R$'),
(19, 'Brunei Darussalam Dollar', 'BND', '$'),
(20, 'Cambodia Riel', 'KHR', '៛'),
(21, 'Canada Dollar', 'CAD', '$'),
(22, 'Cayman Islands Dollar', 'KYD', '$'),
(23, 'Chile Peso', 'CLP', '$'),
(24, 'China Yuan Renminbi', 'CNY', '¥'),
(25, 'Colombia Peso', 'COP', '$'),
(26, 'Costa Rica Colon', 'CRC', '₡'),
(27, 'Croatia Kuna', 'HRK', 'kn'),
(28, 'Cuba Peso', 'CUP', '₱'),
(29, 'Czech Republic Koruna', 'CZK', 'Kč'),
(30, 'Denmark Krone', 'DKK', 'kr'),
(31, 'Dominican Republic Peso', 'DOP', 'RD$'),
(32, 'East Caribbean Dollar', 'XCD', '$'),
(33, 'Egypt Pound', 'EGP', '£'),
(34, 'El Salvador Colon', 'SVC', '$'),
(35, 'EU Euro', 'EUR', '€'),
(36, 'Falkland Islands (Malvinas) Pound', 'FKP', '£'),
(37, 'Fiji Dollar', 'FJD', '$'),
(38, 'Ghana Cedi', 'GHS', '¢'),
(39, 'Gibraltar Pound', 'GIP', '£'),
(40, 'Guatemala Quetzal', 'GTQ', 'Q'),
(41, 'Guernsey Pound', 'GGP', '£'),
(42, 'Guyana Dollar', 'GYD', '$'),
(43, 'Honduras Lempira', 'HNL', 'L'),
(44, 'Hong Kong Dollar', 'HKD', '$'),
(45, 'Hungary Forint', 'HUF', 'Ft'),
(46, 'Iceland Krona', 'ISK', 'kr'),
(47, 'India Rupee', 'INR', ''),
(48, 'Indonesia Rupiah', 'IDR', 'Rp'),
(49, 'Iran Rial', 'IRR', '﷼'),
(50, 'Isle of Man Pound', 'IMP', '£'),
(51, 'Israel Shekel', 'ILS', '₪'),
(52, 'Jamaica Dollar', 'JMD', 'J$'),
(53, 'Japan Yen', 'JPY', '¥'),
(54, 'Jersey Pound', 'JEP', '£'),
(55, 'Kazakhstan Tenge', 'KZT', 'лв'),
(56, 'Korea (North) Won', 'KPW', '₩'),
(57, 'Korea (South) Won', 'KRW', '₩'),
(58, 'Kyrgyzstan Som', 'KGS', 'лв'),
(59, 'Laos Kip', 'LAK', '₭'),
(60, 'Lebanon Pound', 'LBP', '£'),
(61, 'Liberia Dollar', 'LRD', '$'),
(62, 'Macedonia Denar', 'MKD', 'ден'),
(63, 'Malaysia Ringgit', 'MYR', 'RM'),
(64, 'Mauritius Rupee', 'MUR', '₨'),
(65, 'Mexico Peso', 'MXN', '$'),
(66, 'Mongolia Tughrik', 'MNT', '₮'),
(67, 'Mozambique Metical', 'MZN', 'MT'),
(68, 'Namibia Dollar', 'NAD', '$'),
(69, 'Nepal Rupee', 'NPR', '₨'),
(70, 'Netherlands Antilles Guilder', 'ANG', 'ƒ'),
(71, 'New Zealand Dollar', 'NZD', '$'),
(72, 'Nicaragua Cordoba', 'NIO', 'C$'),
(73, 'Nigeria Naira', 'NGN', '₦'),
(74, 'Norway Krone', 'NOK', 'kr'),
(75, 'Oman Rial', 'OMR', '﷼'),
(76, 'Pakistan Rupee', 'PKR', '₨'),
(77, 'Panama Balboa', 'PAB', 'B/.'),
(78, 'Paraguay Guarani', 'PYG', 'Gs'),
(79, 'Peru Sol', 'PEN', 'S/.'),
(80, 'Philippines Piso', 'PHP', '₱'),
(81, 'Poland Zloty', 'PLN', 'zł'),
(82, 'Qatar Riyal', 'QAR', '﷼'),
(83, 'Romania Leu', 'RON', 'lei'),
(84, 'Russia Ruble', 'RUB', '₽'),
(85, 'Saint Helena Pound', 'SHP', '£'),
(86, 'Saudi Arabia Riyal', 'SAR', '﷼'),
(87, 'Serbia Dinar', 'RSD', 'Дин.'),
(88, 'Seychelles Rupee', 'SCR', '₨'),
(89, 'Singapore Dollar', 'SGD', '$'),
(90, 'Solomon Islands Dollar', 'SBD', '$'),
(91, 'Somalia Shilling', 'SOS', 'S'),
(92, 'South Africa Rand', 'ZAR', 'R'),
(93, 'Sri Lanka Rupee', 'LKR', '₨'),
(94, 'Sweden Krona', 'SEK', 'kr'),
(95, 'Switzerland Franc', 'CHF', 'CHF'),
(96, 'Suriname Dollar', 'SRD', '$'),
(97, 'Syria Pound', 'SYP', '£'),
(98, 'Taiwan New Dollar', 'TWD', 'NT$'),
(99, 'Thailand Baht', 'THB', '฿'),
(100, 'Trinidad and Tobago Dollar', 'TTD', 'TT$'),
(101, 'Turkey Lira', 'TRY', ''),
(102, 'Tuvalu Dollar', 'TVD', '$'),
(103, 'Ukraine Hryvnia', 'UAH', '₴'),
(104, 'United Kingdom Pound', 'GBP', '£'),
(105, 'United States Dollar', 'USD', '$'),
(106, 'Uruguay Peso', 'UYU', '$U'),
(107, 'Uzbekistan Som', 'UZS', 'лв'),
(108, 'Venezuela Bolívar', 'VEF', 'Bs'),
(109, 'Viet Nam Dong', 'VND', '₫'),
(110, 'Yemen Rial', 'YER', '﷼'),
(111, 'Zimbabwe Dollar', 'ZWD', 'Z$');

-- --------------------------------------------------------

--
-- Структура таблицы `notify`
--

DROP TABLE IF EXISTS `notify`;
CREATE TABLE `notify` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notify`
--

INSERT INTO `notify` (`id`, `email`) VALUES
(10, 'sd@ml.li');

-- --------------------------------------------------------

--
-- Структура таблицы `otype`
--

DROP TABLE IF EXISTS `otype`;
CREATE TABLE `otype` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `icon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `otype`
--

INSERT INTO `otype` (`id`, `name`, `icon`) VALUES
(1, 'income', 1),
(2, 'expense', 2),
(3, 'transfer', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `language` int(11) NOT NULL,
  `imode` int(11) NOT NULL DEFAULT '1',
  `lastUsedSrcAccount` int(11) NOT NULL DEFAULT '0',
  `lastUsedTgtAccount` int(11) NOT NULL DEFAULT '0',
  `lastUsedIncomeCategory` int(11) NOT NULL DEFAULT '0',
  `lastUsedOutcomeCategory` int(11) NOT NULL DEFAULT '0',
  `showTooltip` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `user`, `language`, `imode`, `lastUsedSrcAccount`, `lastUsedTgtAccount`, `lastUsedIncomeCategory`, `lastUsedOutcomeCategory`, `showTooltip`) VALUES
(237, 3, 1, 1, 8, 9, 16, 17, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `id` bigint(20) NOT NULL,
  `user` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `amount` float NOT NULL DEFAULT '0',
  `second_account` int(11) DEFAULT NULL,
  `reltrans` int(11) DEFAULT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transaction`
--

INSERT INTO `transaction` (`id`, `user`, `date`, `account`, `category`, `amount`, `second_account`, `reltrans`, `comment`) VALUES
(2, 1, '2016-04-30 15:00:00', 5, 0, 0, NULL, NULL, NULL),
(3, 1, '2016-04-30 15:00:00', 1, 0, 0, NULL, NULL, NULL),
(33, 1, '2017-12-02 01:58:58', 1, 13, 6, 0, NULL, '55'),
(47, 1, '2017-12-02 17:42:47', 1, 13, 84, 0, NULL, '9999'),
(74, 1, '2017-11-25 17:38:41', 1, 12, 35000, 0, NULL, 'gffuyuyguyfuyfyfyyfyyuyvvjhjkhguygi'),
(75, 1, '2017-12-03 15:00:00', 1, 10, -350, 0, NULL, 'Травка!!!'),
(84, 1, '2017-11-30 15:00:00', 5, 0, 5000, 1, 85, 'eeeee'),
(85, 1, '2017-11-30 15:00:00', 1, 0, -5000, 5, 84, 'eeeee'),
(86, 1, '2017-12-18 03:55:31', 1, 13, 88, 0, NULL, '88'),
(88, 1, '2017-12-19 03:36:00', 1, 10, -77, 0, NULL, '1212'),
(89, 1, '2017-12-19 03:42:51', 1, 13, 1, 0, NULL, ''),
(91, 1, '2017-12-19 03:47:56', 1, 13, 5, 0, NULL, '13123'),
(92, 1, '2017-12-19 03:48:30', 1, 2, -888, 0, NULL, ''),
(93, 1, '2017-12-28 00:47:00', 1, 13, 1, 0, NULL, '1'),
(94, 1, '2017-12-11 15:00:00', 5, 2, -5, 0, NULL, ''),
(95, 1, '2017-12-28 00:58:53', 1, 13, 7, 0, NULL, ''),
(96, 1, '2017-12-28 05:56:35', 1, 13, 1, 0, NULL, ''),
(97, 1, '2017-11-25 17:38:59', 1, 1, -15000, 0, NULL, 'На бухло....'),
(101, 1, '0000-00-00 00:00:00', 20, 0, 0, NULL, NULL, NULL),
(102, 1, '0000-00-00 00:00:00', 21, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `code` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`, `name`, `active`, `code`) VALUES
(1, 'root', 'e10adc3949ba59abbe56e057f20f883e', 'root@ml.fi', 'ROOt', 1, '555'),
(2, 'qam', '795ff54e894c0d11bdae1ac0c0230542', 'qam@ml.li', '', 0, 'c340af0fcf333aa65568cab5d9058132'),
(3, 'qaz', '4eae18cf9e54a0f62b44176d074cbe2f', 'qaz@qaz.qaz', '', 0, 'b5abdf9ae77c16723651a05b1c199931');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `otype` (`otype`);

--
-- Индексы таблицы `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notify`
--
ALTER TABLE `notify`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u1` (`email`);

--
-- Индексы таблицы `otype`
--
ALTER TABLE `otype`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`),
  ADD KEY `account` (`account`),
  ADD KEY `reltrans` (`reltrans`),
  ADD KEY `category` (`category`),
  ADD KEY `user` (`user`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login` (`login`),
  ADD KEY `password` (`password`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT для таблицы `notify`
--
ALTER TABLE `notify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `otype`
--
ALTER TABLE `otype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=341;

--
-- AUTO_INCREMENT для таблицы `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
