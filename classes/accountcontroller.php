<?php 
include_once ("enums.php");

class AccountController {
    private $sqlController;

    public function GetAccountList($user_id) {
        $query = "select account.id, account.name, account.currency, account.icon,
                    currency.name as curr_name,
                    currency.sign as curr_sign
        from account
            inner join currency
                on currency.id = account.currency        
        where `account`.`user` = $user_id";
        $result = $this->sqlController->ExecuteQuery($query);
        if ($result->num_rows > 0 ) {
            $resData = array();
            while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                $resData[] = $data;
            }
        }
        return json_encode($resData, JSON_UNESCAPED_UNICODE);
    }
    
    public function GetCurrencyList($user_id) {
        $query = "select * from currency";
        $result = $this->sqlController->ExecuteQuery($query);
        if ($result->num_rows > 0 ) {
            $resData = array();
            while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                $resData[] = $data;
            }
        }
        return json_encode($resData, JSON_UNESCAPED_UNICODE);
    }

    public function SaveAccount ($user, $account) {
        $result = 0;
        $query = "UPDATE `account` SET `name`='".$account['name']
        ."',`currency`='".$account['currency']
        ."',`icon`='".$account['icon']
        ."' WHERE `id`=".$account['id']
        ." and `user`=".$user.";";
        $result = $this->sqlController->ExecuteUpdate($query);
        return $result;

        // $query = "UPDATE account SET name = '?', currency = ?, icon = ? WHERE id = ? and user = ?;";
        // $param = array($account['name'], $account['currency'], $account['icon'], $account['id'], $user);
        // $result = $this->sqlController->ExecuteParamUpdateQuery($query, "siiii", $param);
    }

    public function DeleteAccount ($user, $account) {
        $res = 0;
        $query = "select count(id) as cnt from transaction where user = ? and account = ?";
        $param = array($user, $account);
        $result = $this->sqlController->ExecuteParamQuery($query, "ii", $param);
        if ($result && $result->num_rows > 0 ) {
            $data = mysqli_fetch_array ($result, MYSQLI_ASSOC);
            if ($data['cnt'] == 1) {
                $query = "delete from `account` WHERE `user` = ? and `id` = ?";
                $res = $this->sqlController->ExecuteParamUpdateQuery($query, "ii", $param);
            }
        }
        return $res;
    }

    public function CreateAccount ($user, $account) {
        $result = 0;
        $query = "insert into `account` (`name`, `currency`, `icon`, `user`) values (?, ?, ?, ?);";
        $param = array($account['name'], $account['currency'], $account['icon'], $user);
        $id = $this->sqlController->InsertAndReturnId($query, "siii", $param);
        if (0 < $id) {
            $query = "INSERT INTO `transaction`(`user`, `date`, `account`, `category`) VALUES (?, FROM_UNIXTIME(0), ?, 0)";
            $param = array($user, $id);
            $result = $this->sqlController->ExecuteParamUpdateQuery($query, "ii", $param);
        }
        return $result;
    }

    public function __construct($_sql_controller) {
        $this->sqlController = $_sql_controller;
    }
}
?>
