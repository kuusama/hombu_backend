<?php 
include_once ("enums.php");

class CategoryController {
    private $sqlController;

    public function GetCategoryList($user_id) {
        $query = "select category.id, category.name, category.otype, category.icon,
                    otype.name as type_name,
                    otype.icon as type_icon
        from category
            inner join otype
                on otype.id = category.otype        
        where `category`.`user` = $user_id";
        $result = $this->sqlController->ExecuteQuery($query);
        $resData = array();
        if ($result->num_rows > 0 ) {
            while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                $resData[] = $data;
            }
        }
        return json_encode($resData, JSON_UNESCAPED_UNICODE);
    }

    public function GetCategoryListByType($user_id, $cat_type) {
        $query = "select category.id, category.name, category.otype, category.icon,
                    otype.name as type_name,
                    otype.icon as type_icon
        from category
            inner join otype
                on otype.id = category.otype        
        where `category`.`user` = ".$user_id." and category.otype = ".$cat_type;
        $result = $this->sqlController->ExecuteQuery($query);
        $resData = array();
        if ($result->num_rows > 0 ) {
            while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                $resData[] = $data;
            }
        }
        return json_encode($resData, JSON_UNESCAPED_UNICODE);
    }
    
    public function GetTypeList($user_id) {
        $query = "select * from otype";
        $result = $this->sqlController->ExecuteQuery($query);
        $resData = array();
        if ($result->num_rows > 0 ) {
            while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                $resData[] = $data;
            }
        }
        return json_encode($resData, JSON_UNESCAPED_UNICODE);
    }

    public function SaveCategory ($user, $category) {
        $result = 0;
        $query = "UPDATE `category` SET `name`='".$category['name']
        ."',`otype`='".$category['otype']
        ."',`icon`='".$category['icon']
        ."' WHERE `id`=".$category['id']
        ." and `user`=".$user.";";
        $result = $this->sqlController->ExecuteUpdate($query);
        return $result;
    }

    public function DeleteCategory ($user, $category) {
        $res = 0;
        $query = "select count(id) as cnt from transaction where user = ? and category = ?";
        $param = array($user, $category);
        $result = $this->sqlController->ExecuteParamQuery($query, "ii", $param);
        if ($result && $result->num_rows > 0 ) {
            $data = mysqli_fetch_array ($result, MYSQLI_ASSOC);
            if ($data['cnt'] == 0) {
                $query = "delete from `category` WHERE `user` = ? and `id` = ?";
                $res = $this->sqlController->ExecuteParamUpdateQuery($query, "ii", $param);
            }
        }
        return $res;
    }

    public function CreateCategory ($user, $category) {
        $result = 0;
        $query = "insert into `category` (`name`, `otype`, `icon`, `user`)
         values ('".$category['name']."', '".$category['otype']."', '".$category['icon']."', '".$user."');";
        $result = $this->sqlController->ExecuteUpdate($query);
        return $result;
    }

    public function __construct($_sql_controller) {
        $this->sqlController = $_sql_controller;
    }
}
?>
