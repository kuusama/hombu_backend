<?php
class ConSettings {
        public $server;
        public $port;
        public $login;
        public $password;
        public $database;

        public function __construct($_server, $_port, $_login, $_password, $_database) {
            $this->server = $_server;
            $this->port = $_port;
            $this->login = $_login;
            $this->password = $_password;
            $this->database = $_database;
        }
    }
?>