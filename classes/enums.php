<?php
/* Enum definitions */
abstract class Command {
    const Login          = 1;
    const Logout         = 2;
    const StartStream    = 3;
    const StopStream     = 4;
    const SetSetting     = 5;
    const GetSetting     = 6;
    const SetAllSettings = 7;
    const GetAllSettings = 8;

    const GetAccountList = 9;
    const GetCurrencyList = 10;
    const SaveAccount     = 11;
    const DeleteAccount     = 12;
    const CreateAccount     = 13;

    const GetCategoryList  = 14;
    const SaveCategory     = 15;
    const DeleteCategory   = 16;
    const CreateCategory   = 17;
    const GetTypeList      = 18;

    const ConfirmTransaction = 19;
    const EditTransaction    = 21;
    const DeleteTransaction  = 22;

    const GetReport          = 20;

    const SignIn             = 23;
    const NotifyMe           = 24;
}

abstract class TransactionType {
    const Income   = 1;
    const Outcome  = 2;
    const Transfer = 3;
}



class Setting {
    public $type;
    public $value;

    public function __construct($_type, $_value) {
        $this->type  = $_type;
        $this->value = $_value;
    }
}

class AllSettings {
    public $userid;
    public $language;
    public $imode;
    public $useDate;
    public $lastUsedSrcAccount;
    public $lastUsedTgtAccount;
    public $lastUsedIncomeCategory;
    public $lastUsedOutcomeCategory;
    public $showTooltip;
    public $legendType;

    public function __construct($user_id) {
        $this->userid     = $user_id;
        $this->language   = 1;
        $this->imode      = 1;
        $this->useDate    = 1;
        $this->lastUsedSrcAccount = 0;
        $this->lastUsedTgtAccount = 0;
        $this->lastUsedIncomeCategory = 0;
        $this->lastUsedOutcomeCategory = 0;
        $this->showTooltip = 1;
        $this->legendType = 1;
    }
}

?>