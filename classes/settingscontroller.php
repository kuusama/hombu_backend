<?php 
include_once ("enums.php");

class SettingController {
    private $sqlController;

    public function GetSetting($setting) {
		switch ($setting) {
			case SettingType::Language:
				$query = "select setting_value where setting_type=$setting and cam_id=$cam_id";
			break;

			default:
			die("Wrong setting code!");
		}
    }

    public function SaveSetting() {

    }

    private function GetAllSettingsRaw($user_id) {
        $resSetting = new AllSettings($user_id);

        $query = "select * from settings where `user` = $user_id";
        $result = $this->sqlController->ExecuteQuery($query);
        if ($result->num_rows > 0 ) {
            $data = mysqli_fetch_array ($result, MYSQLI_ASSOC);
            $resSetting->language   = $data['language'];
            $resSetting->imode      = $data['imode'];
            $resSetting->useDate    = $data['useDate'];;
            $resSetting->lastUsedSrcAccount = $data['lastUsedSrcAccount'];
            $resSetting->lastUsedTgtAccount = $data['lastUsedTgtAccount'];
            $resSetting->lastUsedIncomeCategory   = $data['lastUsedIncomeCategory'];
            $resSetting->lastUsedOutcomeCategory   = $data['lastUsedOutcomeCategory'];
            $resSetting->showTooltip               = $data['showTooltip'];
            $resSetting->legendType                = $data['legendType'];
        }
        return $resSetting;
    }

    public function GetAllSettings($user_id) {
        return json_encode($this->GetAllSettingsRaw($user_id));
    }

    public function SetAllSettings($settings) {
        $res = $this->sqlController->ExecuteQuery(
            "select `*` from `settings` where `user`=". $settings['userid'] );
        if ($res->num_rows > 0 ) {
            $data = mysqli_fetch_array ($res, MYSQLI_ASSOC);
            $lastSrcAccount = $data['lastUsedSrcAccount'];
            $lastTgtAccount = $data['lastUsedTgtAccount'];
            $lastIncCat     = $data['lastUsedIncomeCategory'];
            $lastUsedOutcomeCategory = $data['lastUsedOutcomeCategory'];
            $settings['lastUsedSrcAccount'] = (isset($settings['lastUsedSrcAccount']) ?  $settings['lastUsedSrcAccount'] : $lastSrcAccount);
            $settings['lastUsedTgtAccount'] = (isset($settings['lastUsedTgtAccount']) ?  $settings['lastUsedTgtAccount'] : $lastTgtAccount);
            $settings['lastUsedIncomeCategory'] = (isset($settings['lastUsedIncomeCategory']) ?  $settings['lastUsedIncomeCategory'] : $lastIncCat);
            $settings['lastUsedOutcomeCategory'] = (isset($settings['lastUsedOutcomeCategory']) ?  $settings['lastUsedOutcomeCategory'] : $lastUsedOutcomeCategory);
            $settings['useDate'] = (isset($settings['useDate']) ?  $settings['useDate'] : $useDate);
        }

        $query = "delete from settings where `user` = ".$settings['userid'];
        $this->sqlController->ExecuteUpdate($query);

        $query1 = "insert into `settings` (`user`, `language`, `lastUsedSrcAccount`,
         `lastUsedTgtAccount`) values (?,?,?,?)";
        $query2 = "update settings set `lastUsedIncomeCategory` = ?, `lastUsedOutcomeCategory` = ?,
        `showTooltip` = ? , `imode` = ?, `legendType` = ?, `useDate` = ? where `user` = ?";
        $param1 = array($settings['userid'], $settings['language'], $settings['lastUsedSrcAccount'], $settings['lastUsedTgtAccount']);
        $param2 = array($settings['lastUsedIncomeCategory'], $settings['lastUsedOutcomeCategory'],
         $settings['showTooltip'], $settings['imode'], $settings['legendType'], $settings['useDate'], $settings['userid']);
        $result = $this->sqlController->ExecuteParamUpdateQuery($query1, "iiii", $param1);
        $result += $this->sqlController->ExecuteParamUpdateQuery($query2, "iiiiiii", $param2);
        return ($result > 0 ? '1' : '0');
    }

    public function __construct($_sql_controller) {
        $this->sqlController = $_sql_controller;
    }
}
?>
