<?php
    class SqlController {
        private $connection;

        private function Connect(ConSettings $conn_settings) {
          //  mysqli_report(MYSQLI_REPORT_ALL);
            $this->connection = new mysqli(
                $conn_settings->server,
                $conn_settings->login,
                $conn_settings->password,
                $conn_settings->database);
            if ($this->connection->connect_error) {
                die('Connect Error (' . $this->connection->connect_errno . ') ' . $this->connection->connect_error);
            }
            if (!$this->connection->set_charset("utf8")) {
                die ($this->connection->error);
            }
        }

        public function ExecuteQuery($query) {
            return $this->connection->query($query);
        }

        public function ExecuteUpdate($query) {
            $this->connection->query($query);
            return $this->connection->affected_rows;
        }

        private function getExecutedStatement($query, $paramTypes, $paramArray) {
            $stmt = $this->connection->prepare($query);
            if(!$stmt) {
                die($this->connection->error);
            }
            //I know that is ugly and I should use "call_user_func_array" here...
            //OK, I swear to refactor this shit later. 
            switch (strlen($paramTypes)) {
                case 1:
                    $stmt->bind_param($paramTypes, $paramArray[0]);
                break;

                case 2:
                    $stmt->bind_param($paramTypes, $paramArray[0], $paramArray[1]);
                break;

                case 3:
                    $stmt->bind_param($paramTypes, $paramArray[0], $paramArray[1], $paramArray[2]);
                break;

                case 4:
                    $stmt->bind_param($paramTypes, $paramArray[0], $paramArray[1], $paramArray[2], $paramArray[3]);
                break;

                case 5:
                    $stmt->bind_param($paramTypes, $paramArray[0], $paramArray[1], $paramArray[2], $paramArray[3], $paramArray[4]);
                break;

                case 6:
                    $stmt->bind_param($paramTypes, $paramArray[0], $paramArray[1], $paramArray[2], $paramArray[3], $paramArray[4], $paramArray[5]);
                break;

                case 7:
                    $stmt->bind_param($paramTypes, $paramArray[0], $paramArray[1], $paramArray[2], $paramArray[3], $paramArray[4], $paramArray[5], $paramArray[6]);
                break;
            }

            $stmt->execute();
            return $stmt;
        }

        public function ExecuteParamQuery($query, $paramTypes, $paramArray) {
            $stmt = $this->getExecutedStatement ($query, $paramTypes, $paramArray);
            return $stmt->get_result();
        }

        public function ExecuteParamUpdateQuery($query, $paramTypes, $paramArray, $die = false) {
            $stmt = $this->getExecutedStatement ($query, $paramTypes, $paramArray);
            if($die) {
                die($stmt->error);
            }
            return $stmt->affected_rows;
        }

        public function InsertAndReturnId($query, $paramTypes, $paramArray) {
            $stmt = $this->getExecutedStatement ($query, $paramTypes, $paramArray);
            return $stmt->insert_id;
        }

        public function __construct(ConSettings $conn_settings) {
            $this->Connect($conn_settings);
        }
    }
?>