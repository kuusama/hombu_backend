<?php
    include_once "./classes/sqlcontroller.php";
    include_once "./classes/enums.php";

    class TransactionController {
        private $sql_controller;

        public function ConfirmTransaction ($user, $transaction) {
            date_default_timezone_set('UTC');
            $result = 0;
            //If edit, just delete previous transaction and create a new one
            if(isset($transaction['id']) && (0 != $transaction['id'])) {
                if ( TransactionType::Transfer == $transaction['type']) {
                    $query = "select `reltrans` from transaction where `user`=? and `id`=?";
                    $param = array($user, $transaction['id']);
                    $result = $this->sql_controller->ExecuteParamQuery($query, 'ii', $param);
                    if ($result->num_rows > 0 ) {
                        $resData[] = mysqli_fetch_array ($result, MYSQLI_ASSOC);
                        $query = "delete from transaction where `user`=? and `id` IN (?, ?)";
                        $param = array($user, $transaction['id'], $resData[0]['reltrans'] );
                        $result = $this->sql_controller->ExecuteParamUpdateQuery($query, 'iii', $param);
                    }
                } else {
                    $query = "delete from transaction where `user`= ? and `id`= ?";
                    $param = array($user, $transaction['id']);
                    $result = $this->sql_controller->ExecuteParamUpdateQuery($query, 'ii', $param);
                }
                if (1 > $result) {
                    die ("SQL ERROR!!!");
                }
            }

            $query1 = "";
            $query2 = "";

            switch ($transaction['type']) {
                case TransactionType::Outcome:
                case TransactionType::Income:
                    $query1 = "INSERT INTO `transaction`
                    (`user`, `date`, `account`, `category`, `amount`, `second_account`, `comment`)
                    VALUES
                    (?, FROM_UNIXTIME(?), ?, ?, ? , 0, ?);";
                    
                    $param = array($user, $transaction['date'], $transaction['sourceAccount'], $transaction['category']
                            , $transaction['amount'], $transaction['comment'] );
                    $result =  $this->sql_controller->ExecuteParamUpdateQuery($query1, 'iiiids', $param);
                break;

                case TransactionType::Transfer:
                    $query1 = "INSERT INTO `transaction` 
                    (`user`, `date`, `account`, `category`, `amount`, `second_account`, `comment`)
                    VALUES (?, FROM_UNIXTIME(?), ?, 0, ?, ?, ?);";

                    $param1 = array($user, $transaction['date'], $transaction['sourceAccount'], 
                                    (-1 * $transaction['amount']), $transaction['targetAccount'], $transaction['comment'] );

                    $query2 = "INSERT INTO `transaction`
                    (`user`, `date`, `second_account`, `category`, `amount`, `account`, `comment`)
                    VALUES (?, FROM_UNIXTIME(?), ?, 0, ?, ?, ?);";
                    
                    $param2 = array($user, $transaction['date'], $transaction['sourceAccount'],
                                    $transaction['amount'], $transaction['targetAccount'], $transaction['comment']);

                    $id1 =  $this->sql_controller->InsertAndReturnId($query1, 'iiidis', $param1);
                    $id2 =  $this->sql_controller->InsertAndReturnId($query2, 'iiidis', $param2);

                    $query3 = "update transaction set `reltrans`=? where `id`=?";

                    $result =  $this->sql_controller->ExecuteParamUpdateQuery($query3, 'ii', array($id2, $id1));
                    $result +=  $this->sql_controller->ExecuteParamUpdateQuery($query3, 'ii', array($id1, $id2));
                break;

                default:
                break;
            }
            return $result;
        }

        public function DeleteTransaction ($user, $transaction) {
            date_default_timezone_set('UTC');
            $result = 0;
            if(isset($transaction['id']) && (0 != $transaction['id'])) {
                if ( TransactionType::Transfer == $transaction['type']) {
                    $query = "select `reltrans` from transaction where `user`=? and `id`=?";
                    $result = $this->sql_controller->ExecuteParamQuery($query, "ii", array($user, $transaction['id']));
                    if ($result->num_rows > 0 ) {
                        $resData[] = mysqli_fetch_array ($result, MYSQLI_ASSOC);
                        $query = "delete from transaction where `user`=? and `id` IN (?, ?)";
                        $result = $this->sql_controller->ExecuteParamUpdateQuery($query, "iii", array($user, $transaction['id'], $resData[0]['reltrans']));
                    }
                } else {
                    $query = "delete from transaction where `user`=? and `id`=?";
                    $result = $this->sql_controller->ExecuteParamUpdateQuery($query, "ii", array($user, $transaction['id']));
                }
                if (1 > $result) {
                    die ("SQL ERROR!!!");
                }
            }
            return $result;
        }

        public function GetTransaction ($user, $trans_id) {
            date_default_timezone_set('UTC');
            $query = "select id,
              `transaction`.`date` as date,
              0 as type,
              `account` as sourceAccount,
              `second_account` as targetAccount,
              `category`, `amount`, `comment`
              from transaction where `id`=? and `user` = ?";
            $result = $this->sql_controller->ExecuteParamQuery($query, "ii", array($trans_id, $user));
            $resData = array();
            if ($result->num_rows > 0 ) {
                $resData[] = mysqli_fetch_array ($result, MYSQLI_ASSOC);
                $resData[0]['type'] = ($resData[0]['targetAccount'] == 0 ? ($resData[0]['amount'] > 0 ? 1 : 2 ) : 3 );
            }
            return json_encode($resData, JSON_UNESCAPED_UNICODE);
        }

        public function GetBasicReport($user, $from, $to, $itype) {
            date_default_timezone_set('UTC');
            $query = "";

            if (2 != $itype) {
                //Начальный остаток
                $query = "select `id`, `name` as account, SUM(BGN) as begin, SUM(INCOME) as income, SUM(EXPENSE) as expense, SUM(ENDD) as end
                FROM
                (
                    (SELECT 'B' as GRP, `account`.`id`, `account`.`name`, SUM(`transaction`.`amount`) as BGN, 0 as INCOME, 0 as EXPENSE, 0 AS ENDD
                            FROM `transaction`
                                INNER JOIN `account` ON
                                    `account`.`id` = `transaction`.`account`
                                     and `account`.`user` = `transaction`.`user`
                            WHERE `transaction`.`user` = $user
                                and UNIX_TIMESTAMP(`transaction`.`date`) < $from
                            GROUP BY `transaction`.`account`)

                        UNION

                        (SELECT 'I' as GRP, `account`.`id`, `account`.`name`, 0 AS BGN, SUM(`transaction`.`amount`) AS INCOME, 0 as oUTCOME , 0 AS ENDD
                            FROM `transaction`
                                INNER JOIN `account` ON
                                    `account`.`id` = `transaction`.`account`
                                     and `account`.`user` = `transaction`.`user`
                            WHERE `transaction`.`user` = $user
                                and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                                and `transaction`.`amount` > 0
                                GROUP BY `transaction`.`account`)
                        UNION

                        (SELECT 'O' as GRP, `account`.`id`, `account`.`name`, 0 AS BGN, 0 AS INCOME, IFNULL(SUM(`transaction`.`amount`), 0) AS EXPENSE, 0 AS ENDD
                        FROM `transaction`
                            INNER JOIN `account` ON
                                `account`.`id` = `transaction`.`account`
                                 and `account`.`user` = `transaction`.`user`
                        WHERE `transaction`.`user` = $user
                            and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                            and `transaction`.`amount` < 0
                            GROUP BY `transaction`.`account`)

                        UNION

                        (SELECT 'E' as GRP, `account`.`id`, `account`.`name`, 0 AS BGN, 0 AS INCOME, 0 AS EXPENSE, SUM(`transaction`.`amount`) AS ENDD
                            FROM `transaction`
                                INNER JOIN `account` ON
                                    `account`.`id` = `transaction`.`account`
                                     and `account`.`user` = `transaction`.`user`
                            WHERE `transaction`.`user` = $user
                                and UNIX_TIMESTAMP(`transaction`.`date`) <= $to
                                GROUP BY `transaction`.`account`)
                                ) q1
                    GROUP BY `id`, `name`
            union   
                    select 0, 'ac_total' as account, SUM(BGN) as begin , SUM(INCOME) as income, SUM(EXPENSE) as expense, SUM(ENDD) as end
                    FROM
                    (
                        (SELECT 'B' as GRP, `account`.`name`, SUM(`transaction`.`amount`) as BGN, 0 as INCOME, 0 as EXPENSE, 0 AS ENDD
                                FROM `transaction`
                                    INNER JOIN `account` ON
                                        `account`.`id` = `transaction`.`account`
                                         and `account`.`user` = `transaction`.`user`
                                WHERE `transaction`.`user` = $user
                                    and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)
                                    and UNIX_TIMESTAMP(`transaction`.`date`) < $from
                                GROUP BY `transaction`.`account`)
                    
                            UNION
                    
                            (SELECT 'I' as GRP, `account`.`name`, 0 AS BGN, SUM(`transaction`.`amount`) AS INCOME, 0 as oUTCOME , 0 AS ENDD
                                FROM `transaction`
                                    INNER JOIN `account` ON
                                        `account`.`id` = `transaction`.`account`
                                         and `account`.`user` = `transaction`.`user`
                                WHERE `transaction`.`user` = $user
                                    and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)
                                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                                    and `transaction`.`amount` > 0
                                    GROUP BY `transaction`.`account`)
                            UNION
                    
                            (SELECT 'O' as GRP, `account`.`name`, 0 AS BGN, 0 AS INCOME, IFNULL(SUM(`transaction`.`amount`), 0) AS EXPENSE, 0 AS ENDD
                            FROM `transaction`
                                INNER JOIN `account` ON
                                    `account`.`id` = `transaction`.`account`
                                     and `account`.`user` = `transaction`.`user`
                            WHERE `transaction`.`user` = $user
                                and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)
                                and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                                and `transaction`.`amount` < 0
                                GROUP BY `transaction`.`account`)
                        UNION
                    
                            (SELECT 'E' as GRP, `account`.`name`, 0 AS BGN, 0 AS INCOME, 0 AS EXPENSE, SUM(`transaction`.`amount`) AS ENDD
                                FROM `transaction`
                                    INNER JOIN `account` ON
                                        `account`.`id` = `transaction`.`account`
                                         and `account`.`user` = `transaction`.`user`
                                WHERE `transaction`.`user` = $user
                                    and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)
                                    and UNIX_TIMESTAMP(`transaction`.`date`) <= $to
                                    GROUP BY `transaction`.`account`)
                                    ) q2
                                ";
            } else {
                $query = "select 0, 'ac_total' as account, SUM(BGN) as begin , SUM(INCOME) as income, SUM(EXPENSE) as expense, SUM(ENDD) as end
                    FROM
                    (
                        (SELECT 'B' as GRP, `account`.`name`, SUM(`transaction`.`amount`) as BGN, 0 as INCOME, 0 as EXPENSE, 0 AS ENDD
                                FROM `transaction`
                                    INNER JOIN `account` ON
                                        `account`.`id` = `transaction`.`account`
                                         and `account`.`user` = `transaction`.`user`
                                WHERE `transaction`.`user` = $user
                                    and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)
                                    and UNIX_TIMESTAMP(`transaction`.`date`) < $from
                                GROUP BY `transaction`.`account`)
                    
                            UNION
                    
                            (SELECT 'I' as GRP, `account`.`name`, 0 AS BGN, SUM(`transaction`.`amount`) AS INCOME, 0 as oUTCOME , 0 AS ENDD
                                FROM `transaction`
                                    INNER JOIN `account` ON
                                        `account`.`id` = `transaction`.`account`
                                         and `account`.`user` = `transaction`.`user`
                                WHERE `transaction`.`user` = $user
                                    and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)
                                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                                    and `transaction`.`amount` > 0
                                    GROUP BY `transaction`.`account`)
                            UNION
                    
                            (SELECT 'O' as GRP, `account`.`name`, 0 AS BGN, 0 AS INCOME, IFNULL(SUM(`transaction`.`amount`), 0) AS EXPENSE, 0 AS ENDD
                            FROM `transaction`
                                INNER JOIN `account` ON
                                    `account`.`id` = `transaction`.`account`
                                     and `account`.`user` = `transaction`.`user`
                            WHERE `transaction`.`user` = $user
                                and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)
                                and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                                and `transaction`.`amount` < 0
                                GROUP BY `transaction`.`account`)
                        UNION
                    
                            (SELECT 'E' as GRP, `account`.`name`, 0 AS BGN, 0 AS INCOME, 0 AS EXPENSE, SUM(`transaction`.`amount`) AS ENDD
                                FROM `transaction`
                                    INNER JOIN `account` ON
                                        `account`.`id` = `transaction`.`account`
                                         and `account`.`user` = `transaction`.`user`
                                WHERE `transaction`.`user` = $user
                                    and (`transaction`.`second_account` = 0 or `transaction`.`second_account` = NULL)
                                    and UNIX_TIMESTAMP(`transaction`.`date`) <= $to
                                    GROUP BY `transaction`.`account`)
                                    ) q2";
            }

            $result = $this->sql_controller->ExecuteQuery($query);
            $resData = array();
            if ($result->num_rows > 0 ) {
                while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                    $resData[] = $data;
                }
            }
            return json_encode($resData, JSON_UNESCAPED_UNICODE);
        }

        public function GetAccountReport($user, $from, $to, $account, $itype) {
            date_default_timezone_set('UTC');
            $query = "";
            if (2 != $itype) {
                $query = "
                SELECT `category`.`id`, `category`.`name`, SUM(`transaction`.`amount`) AS income, ' ' AS expense, `category`.`otype`
                    FROM `transaction`
                        INNER JOIN `category` ON
                            `category`.`id` = `transaction`.`category`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                        and `transaction`.`amount` > 0
                        and `transaction`.`account` = $account
                    GROUP BY `transaction`.`category`
    
                UNION
    
                SELECT `category`.`id`, `category`.`name`, ' ' as income, -1 * SUM(`transaction`.`amount`) AS expense, `category`.`otype`
                    FROM `transaction`
                        INNER JOIN `category` ON
                            `category`.`id` = `transaction`.`category`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                        and `transaction`.`amount` < 0
                        and `transaction`.`account` = $account
                    GROUP BY `transaction`.`category`
                
                UNION
    
                SELECT 0, `account`.`name`, SUM(`transaction`.`amount`) AS income, ' ' AS expense, '3' as otype
                FROM `transaction`
                    INNER JOIN `account` ON
                        `account`.`id` = `transaction`.`second_account`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                    and `transaction`.`amount` > 0
                    and `transaction`.`account` = $account
                GROUP BY `transaction`.`second_account`
    
                UNION
    
                SELECT 0, `account`.`name`, ' ' as income, -1 * SUM(`transaction`.`amount`) AS expense, '3' as otype
                FROM `transaction`
                    INNER JOIN `account` ON
                    `account`.`id` = `transaction`.`second_account`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                    and `transaction`.`amount` < 0
                    and `transaction`.`account` = $account
                GROUP BY `transaction`.`second_account`";
            } else {
                $query = "
                SELECT `category`.`id`, `category`.`name`, SUM(`transaction`.`amount`) AS income, ' ' AS expense, `category`.`otype`
                    FROM `transaction`
                        INNER JOIN `category` ON
                            `category`.`id` = `transaction`.`category`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                        and `transaction`.`amount` > 0
                    GROUP BY `transaction`.`category`
    
                UNION
    
                SELECT `category`.`id`, `category`.`name`, ' ' as income, -1 * SUM(`transaction`.`amount`) AS expense, `category`.`otype`
                    FROM `transaction`
                        INNER JOIN `category` ON
                            `category`.`id` = `transaction`.`category`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                        and `transaction`.`amount` < 0
                    GROUP BY `transaction`.`category`";
            }
            

            $result = $this->sql_controller->ExecuteQuery($query);
            $resData = array();
            if ($result->num_rows > 0 ) {
                while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                    $resData[] = $data;
                }
            }
            return json_encode($resData, JSON_UNESCAPED_UNICODE);
        }

        public function GetAccountPiechart($user, $from, $to, $account, $itype) {
            date_default_timezone_set('UTC');
            $query = "";
            if ($account > 0) {
                $query = "
                SELECT `category`.`id`, `category`.`name`, SUM(`transaction`.`amount`) AS income, ' ' AS expense, `category`.`otype` , 0 as transfer, `category`.`icon` as icon
                    FROM `transaction`
                        INNER JOIN `category` ON
                            `category`.`id` = `transaction`.`category`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                        and `transaction`.`amount` > 0
                        and `transaction`.`account` = $account
                    GROUP BY `transaction`.`category`
    
                UNION
    
                SELECT `category`.`id`, `category`.`name`, ' ' as income, -1 * SUM(`transaction`.`amount`) AS expense, `category`.`otype`, 0 as transfer, `category`.`icon` as icon
                    FROM `transaction`
                        INNER JOIN `category` ON
                            `category`.`id` = `transaction`.`category`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                        and `transaction`.`amount` < 0
                        and `transaction`.`account` = $account
                    GROUP BY `transaction`.`category`
                
                UNION

                SELECT `transaction`.`second_account` as `id`, `account`.`name`, SUM(`transaction`.`amount`) AS income, ' ' AS expense, 1 as `otype`, 1 as transfer, `account`.`icon` as icon
                FROM `transaction`
                    INNER JOIN `account` ON
                        `account`.`id` = `transaction`.`second_account`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                    and `transaction`.`amount` > 0
                    and `transaction`.`account` = $account
                    and `transaction`.`category` = 0
                GROUP BY `transaction`.`second_account`

                UNION

                SELECT `transaction`.`second_account` as `id`, `account`.`name`, ' ' as income, -1 * SUM(`transaction`.`amount`) AS expense, 2 as `otype`, 1 as transfer, `account`.`icon` as icon
                    FROM `transaction`
                        INNER JOIN `account` ON
                            `account`.`id` = `transaction`.`second_account`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                        and `transaction`.`amount` < 0
                        and `transaction`.`account` = $account
                        and `transaction`.`category` = 0
                    GROUP BY `transaction`.`second_account`
                
                UNION

                select  0 as `id`, 'total' as `name`, SUM(`income`) as income, SUM(`expense`) as `expense`, 4 as otype, 0 as transfer , 0 as icon from
                (

                    SELECT 0 as `id`, `account`.`name` as `name`, SUM(`transaction`.`amount`) as income, 0 as `expense`, 4 as otype
                    FROM `transaction`
                        INNER JOIN `account` ON
                            `account`.`id` = `transaction`.`account`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) < $from
                        and `transaction`.`account` = $account

                    UNION

                    SELECT 0 as `id`, `account`.`name` as `name`,  0 as `income`, SUM(`transaction`.`amount`) as expense, 4 as otype
                    FROM `transaction`
                        INNER JOIN `account` ON
                            `account`.`id` = `transaction`.`account`
                    WHERE `transaction`.`user` = $user
                        and UNIX_TIMESTAMP(`transaction`.`date`) <= $to
                        and `transaction`.`account` = $account
                ) q2
                    ";
            } else {
                $query = "
                SELECT `category`.`id`, `category`.`name`, SUM(`transaction`.`amount`) AS income, ' ' AS expense, `category`.`otype`, `category`.`icon` as icon
                FROM `transaction`
                    INNER JOIN `category` ON
                        `category`.`id` = `transaction`.`category`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                    and `transaction`.`amount` > 0
                GROUP BY `transaction`.`category`

            UNION

            SELECT `category`.`id`, `category`.`name`, ' ' as income, -1 * SUM(`transaction`.`amount`) AS expense, `category`.`otype`, `category`.`icon` as icon
                FROM `transaction`
                    INNER JOIN `category` ON
                        `category`.`id` = `transaction`.`category`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                    and `transaction`.`amount` < 0
                GROUP BY `transaction`.`category`
            
            UNION

            select  0 as `id`, 'total' as `name`, SUM(`income`) as income, SUM(`expense`) as `expense`, 4 as otype, 0 as icon from
            (

                SELECT 0 as `id`, `account`.`name` as `name`, SUM(`transaction`.`amount`) as income, 0 as `expense`, 4 as otype
                FROM `transaction`
                    INNER JOIN `account` ON
                        `account`.`id` = `transaction`.`account`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) < $from
                GROUP BY `id`, `name`
                
                UNION
                
                SELECT 0 as `id`, `account`.`name` as `name`,  0 as `income`, SUM(`transaction`.`amount`) as expense, 4 as otype
                FROM `transaction`
                    INNER JOIN `account` ON
                        `account`.`id` = `transaction`.`account`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) <= $to
                GROUP BY `id`, `name`
            ) q2";
            }

            $result = $this->sql_controller->ExecuteQuery($query);
            $resData = array();
            if ($result->num_rows > 0 ) {
                while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                    $resData[] = $data;
                }
            }
            return json_encode($resData, JSON_UNESCAPED_UNICODE);
        }

        public function GetCategoryReport($user, $from, $to, $account, $category) {
            date_default_timezone_set('UTC');
            $account_filter = (0 < $account ? " and `transaction`.`account` = $account" : ""); 
            $query = "
            (SELECT `transaction`.`id`, DATE_FORMAT(`transaction`.`date`, \"%d.%m.%Y\") as date, `transaction`.`amount` AS income, '' AS expense, `transaction`.`comment`
                FROM `transaction`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                    and `transaction`.`amount` > 0
                    and `transaction`.`category` = $category
                    $account_filter
            ORDER BY `transaction`.`date`)

            UNION

            (SELECT `transaction`.`id`, DATE_FORMAT(`transaction`.`date`, \"%d.%m.%Y\") as date, '' as income, -1 * `transaction`.`amount` AS expense, `transaction`.`comment`
                FROM `transaction`
                WHERE `transaction`.`user` = $user
                    and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN $from AND $to
                    and `transaction`.`amount` < 0
                    and `transaction`.`category` = $category
                    $account_filter 
                ORDER BY `transaction`.`date`)";

            $result = $this->sql_controller->ExecuteQuery($query);
            $resData = array();
            if ($result->num_rows > 0 ) {
                while ($data = mysqli_fetch_array ($result, MYSQLI_ASSOC)) {
                    $resData[] = $data;
                }
            }
            return json_encode($resData, JSON_UNESCAPED_UNICODE);
        }

        public function GetBalanceChart($user, $from, $to, $type) {
            date_default_timezone_set('UTC');
            $this->sql_controller->ExecuteQuery("set @JSN = ''");
            $query = ("CALL `getBalances`($user, FROM_UNIXTIME($from), FROM_UNIXTIME($to), $type, @JSN);");
            $result = $this->sql_controller->ExecuteQuery($query);
            $row = $result->fetch_assoc();
            return $row['jSON'];
        }

        public function GetBalanceChartDetail($user, $from, $to, $type) {
            date_default_timezone_set('UTC');
            $this->sql_controller->ExecuteQuery("set @JSN = ''");
            $query = ("CALL `getBalancesDetail`($user, FROM_UNIXTIME($from), FROM_UNIXTIME($to), $type, @JSN);");
            $result = $this->sql_controller->ExecuteQuery($query);
            $row = $result->fetch_assoc();
            return $row['jSON'];
        }

        public function GetIncomeChart($user, $from, $to, $type, $repType) {
            date_default_timezone_set('UTC');
            error_reporting( E_ALL );
            $interval = '';
            switch ($type) {
                case 1:
                    $interval = 'P1D';
                break;
                case 2:
                    $interval = 'P1W';
                break;
                case 3:
                    $interval = 'P1M';
                break;
                case 4:
                    $interval = 'P1Y';
                break;
            }
            $dateFrom = new DateTime(); $dateFrom->setTimestamp($from);
            $dateTo   = new DateTime(); $dateTo->setTimestamp($to);

            /* Get all categories */
            $catQuery  = "select id, name from category where otype = ".($repType == 8 ? "1" : "2")." and user =$user";
            $catResult = $this->sql_controller->ExecuteQuery($catQuery);
            $resArray = [];
            $dateArray = [];
            $categoryArray = [];
            $firstCycle = true;

            while ($data = mysqli_fetch_array ($catResult, MYSQLI_ASSOC)) {
                $currentData = [];
                $currentCatId   = $data['id'];
                $currentCatName = $data['name'];
                $categoryArray[] = $currentCatName;

                $currentDateFrom = new DateTime(); $currentDateFrom->setTimestamp($from);
                $currentDateTo   = new DateTime(); $currentDateTo->setTimestamp($from);
                $currentDateTo->add(new DateInterval($interval));
                while ($currentDateTo < $dateTo) {
                    $dateString = $currentDateTo->format('d.m.Y');
                    if ($firstCycle) {
                        $dateArray[] = $dateString;
                    }
                    $incomeQuery = "select SUM(amount) as `income` from transaction where amount ".($repType == 8 ? ">" : "<")." 0 and user = $user and
                     UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN ".$currentDateFrom->getTimestamp().
                     " AND ".$currentDateTo->getTimestamp()." and category = $currentCatId";
                    $sumResult = $this->sql_controller->ExecuteQuery($incomeQuery);
                    $income = $sumResult->fetch_assoc()['income'];
                    $currentData[] = (null != $income ? $income * ($repType == 8 ? 1 : -1) : 0 );

                    $currentDateFrom->add(new DateInterval($interval));
                    $currentDateTo->add(new DateInterval($interval));
                }
                $currentDateFrom = $currentDateTo;
                $currentDateTo   = $dateTo;
                $dateString = $currentDateTo->format('d.m.Y');
                if ($firstCycle) {
                    $dateArray[] = $dateString;
                }
                $incomeQuery = "select SUM(amount) as `income` from transaction where amount > 0 and user = $user and
                 UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN ".$currentDateFrom->getTimestamp().
                 " AND ".$currentDateTo->getTimestamp()." and category = $currentCatId";
                $sumResult = $this->sql_controller->ExecuteQuery($incomeQuery);
                $income = $sumResult->fetch_assoc()['income'];
                $currentData[] = (null != $income ? $income : 0 );
                $resArray[] = $currentData;
                $firstCycle = false;
            }

            return Array('cat' => $categoryArray, 'dates' => $dateArray, 'incomes' => $resArray);
        }

        public function GetTotalIncomeChart($user, $from, $to, $type, $repType) {
            date_default_timezone_set('UTC');
            $interval = '';
            switch ($type) {
                case 1:
                    $interval = 'P1D';
                break;
                case 2:
                    $interval = 'P1W';
                break;
                case 3:
                    $interval = 'P1M';
                break;
                case 4:
                    $interval = 'P1Y';
                break;
            }
            $dateFrom = new DateTime(); $dateFrom->setTimestamp($from);
            $dateTo   = new DateTime(); $dateTo->setTimestamp($to);

            $resArray = [];
            $dateArray = [];
            $categoryArray = [];
                $currentData = [];
                $categoryArray[] = 'ac_total';

                $currentDateFrom = new DateTime(); $currentDateFrom->setTimestamp($from);
                $currentDateTo   = new DateTime(); $currentDateTo->setTimestamp($from);
                $currentDateTo->add(new DateInterval($interval));
                while ($currentDateTo < $dateTo) {
                    $dateString = $currentDateTo->format('d.m.Y');
                    $dateArray[] = $dateString;
                    $incomeQuery = "select SUM(amount) as `income` from transaction where amount ".($repType == 10 ? ">" : "<")." 0 and user = $user and
                     UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN ".$currentDateFrom->getTimestamp().
                     " AND ".$currentDateTo->getTimestamp()." and category <> 0";
                    $sumResult = $this->sql_controller->ExecuteQuery($incomeQuery);
                    $income = $sumResult->fetch_assoc()['income'];
                    $currentData[] = (null != $income ? $income * ($repType == 10 ? 1 : -1) : 0 );
                    $currentDateFrom->add(new DateInterval($interval));
                    $currentDateTo->add(new DateInterval($interval));
                }
                $currentDateFrom = $currentDateTo;
                $currentDateTo   = $dateTo;
                $dateString = $currentDateTo->format('d.m.Y');
                $dateArray[] = $dateString;
                $incomeQuery = "select SUM(amount) as `income` from transaction where amount > 0 and user = $user and
                 UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN ".$currentDateFrom->getTimestamp().
                 " AND ".$currentDateTo->getTimestamp()." and category <> 0";
                $sumResult = $this->sql_controller->ExecuteQuery($incomeQuery);
                $income = $sumResult->fetch_assoc()['income'];
                $currentData[] = (null != $income ? $income : 0 );
                $resArray[] = $currentData;
                $firstCycle = false;

            return Array('cat' => $categoryArray, 'dates' => $dateArray, 'incomes' => $resArray);
        }

        public function  __construct($_sqlcontroller) {
            $this->sql_controller = $_sqlcontroller;
            date_default_timezone_set("UTC"); 
        }
    }
?>