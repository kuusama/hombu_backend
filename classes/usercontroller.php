<?php
    include_once "./classes/sqlcontroller.php";

    class UserController {
        private $sql_controller;

        public function confirm ($code) {
            $query = "select COUNT(`id`) as `cnt` from `users` where `code`= ?";
            $param = array($code);
            $res = $this->sql_controller->ExecuteParamQuery($query, 's', $param);

            $data = mysqli_fetch_array ($res, MYSQLI_ASSOC);
            if ($data['cnt'] == 1) {
                //user exists, try to make him active
                $query = "update `users` set `active` = 1, `code` = '' where `code` = ?";
                return $this->sql_controller->ExecuteParamUpdateQuery($query, 's', $param);
            }
            return 0;
        }

        public function NotifyMe ($email) {
            $query = "insert into `notify` (`email`) values (?);";
            $param = array($email);
            return $this->sql_controller->ExecuteParamUpdateQuery($query, 's', $param);
        }

        public function authenticate ($user) {
            $result = 0;
            $query = "select `id`, `active` from users where `login`=? and `password` = ?";
            $param = array($user->login, $user->password);
            $res = $this->sql_controller->ExecuteParamQuery($query, 'ss', $param);
            if ($res && $res->num_rows > 0 ) {
                $data = mysqli_fetch_array ($res, MYSQLI_ASSOC);
                $result = ($data['active'] == 1 ? $data['id'] : -1);
            }
            return $result;
        }

        public function signIn($user) {
            $result = 0;
            $query = "select COUNT(`id`) as `cnt` from users where `email`=?;";
            $res = $this->sql_controller->ExecuteParamQuery($query, 's', array($user->email));
            $data = mysqli_fetch_array ($res, MYSQLI_ASSOC);
            if ($data['cnt'] > 0) {
                return 1;
            }

            $code = md5(openssl_random_pseudo_bytes (45));
            $query = "insert into users (`login`, `password`, `email`, `active`, `code`) values (?, ?, ?, ?, ?)";
            $param = array($user->login, $user->password, $user->email, 0, $code);
            $newUserId = $this->sql_controller->InsertAndReturnId($query, 'sssis', $param);

            $param = array($newUserId);
            $query = "INSERT INTO `account`(`user`, `name`, `currency`, `icon`) VALUES (?,'Main account',105,1)";
            $newAcc1 = $this->sql_controller->InsertAndReturnId($query, 'i', $param);
            $query = "INSERT INTO `account`(`user`, `name`, `currency`, `icon`) VALUES (?,'Second account',105,1)";
            $newAcc2 = $this->sql_controller->InsertAndReturnId($query, 'i', $param);

            $query = "INSERT INTO `category`(`user`, `name`, `otype`, `icon`) VALUES (?,'Sample income category',1,1)";
            $newCat1 = $this->sql_controller->InsertAndReturnId($query, 'i', $param);
            $query = "INSERT INTO `category`(`user`, `name`, `otype`, `icon`) VALUES (?,'Sample expense category',2,2)";
            $newCat2 = $this->sql_controller->InsertAndReturnId($query, 'i', $param);

            $query = "INSERT INTO `settings`(`user`, `language`, `imode`, `lastUsedSrcAccount`, `lastUsedTgtAccount`, 
            `lastUsedIncomeCategory`, `lastUsedOutcomeCategory`) VALUES (?,1,1,?,?,?,?)";
            $this->sql_controller->ExecuteParamQuery($query, 'iiiii', array($newUserId, $newAcc1, $newAcc2, $newCat1, $newCat2));

            $query = "INSERT INTO `transaction`(`user`, `date`, `account`, `category`, `amount`) 
            VALUES (?, FROM_UNIXTIME(0), ?, 0, 0)";
            $this->sql_controller->ExecuteParamQuery($query, 'ii', array($newUserId, $newAcc1));
            $this->sql_controller->ExecuteParamQuery($query, 'ii', array($newUserId, $newAcc2));

            $headers = 'From: register@litemoney.online' . "\r\n";
            $headers .= "Reply-To: register@litemoney.online" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            $message = "Someone want to sign up on litemoney.online via your email.<br>" ."\r\n"
            ."To confirm registration click the following link: "
            ."<a href=litemoney.online/backend/index.php?confirm=$code>Confirm registration</a> \r\n";

            $result = 3;
            if (false != mail($user->email, "Litemoney.online registration",
             $message,
             $headers, '-fregister@litemoney.online')) {
                $result = 0;
             }
            return $result;
        }

        public function  __construct($_sqlcontroller) {
            $this->sql_controller = $_sqlcontroller;
        }
    }
?>