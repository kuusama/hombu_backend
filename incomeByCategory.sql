BEGIN
 DECLARE done TINYINT DEFAULT FALSE;
 DECLARE done0 TINYINT DEFAULT FALSE;
 DECLARE v_date DATE;
 DECLARE v_balance FLOAT;
 DECLARE v_category VARCHAR(255);
 DECLARE c_name VARCHAR(255);
 DECLARE c_id INT;
 DECLARE cursCat CURSOR FOR SELECT `id`, `name` from `category` order by `name`;
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done0 = TRUE;

DROP TABLE IF EXISTS `tmptable`; 
CREATE TABLE `tmptable` (date1 DATE, balance FLOAT, category VARCHAR(255) ) ENGINE = MEMORY;

OPEN cursCat;
cat_loop:
 LOOP
  FETCH FROM cursCat INTO c_id, c_name;

  IF done0 THEN 
    LEAVE cat_loop;
  ELSE
    BEGIN
        set @currDateFrom = dateFrom;
        IF intervalType = 1 THEN
            SET @currDateTo := DATE_ADD(@currDateFrom, INTERVAL 1 DAY);
        ELSEIF intervalType = 2 THEN
            SET @currDateTo := DATE_ADD(@currDateFrom, INTERVAL 1 WEEK);
        ELSEIF intervalType = 3 THEN
            SET @currDateTo := DATE_ADD(@currDateFrom, INTERVAL 1 MONTH);
        ELSEIF intervalType = 4 THEN
            SET @currDateTo := DATE_ADD(@currDateFrom, INTERVAL 1 YEAR);
        END IF;

        WHILE @currDateTo < dateTo DO
            set @strTimestampFrom := UNIX_TIMESTAMP(@currDateFrom);
            set @strTimestampTo := UNIX_TIMESTAMP(@currDateTo);

            set @query2 = concat('insert into tmptable SELECT \'', DATE_FORMAT(@currDateTo,'%Y-%m-%d'), '\', IFNULL(SUM(`transaction`.`amount`),0), `category`.`name` as `category` FROM `transaction` INNER JOIN `category` on `category`.`id` = `transaction`.`category` WHERE `transaction`.`amount` > 0 and `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN ', @strTimestampFrom ,' AND ', @strTimestampTo , ' GROUP BY `category`;');
            PREPARE tmpQ from @query2;
            EXECUTE tmpQ;

            set @currDateFrom = @currDateTo;
            IF intervalType = 1 THEN
                SET @currDateTo := DATE_ADD(@currDateFrom, INTERVAL 1 DAY);
            ELSEIF intervalType = 2 THEN
                SET @currDateTo := DATE_ADD(@currDateFrom, INTERVAL 1 WEEK);
            ELSEIF intervalType = 3 THEN
                SET @currDateTo := DATE_ADD(@currDateFrom, INTERVAL 1 MONTH);
            ELSEIF intervalType = 4 THEN
                SET @currDateTo := DATE_ADD(@currDateFrom, INTERVAL 1 YEAR);
            END IF;
        END WHILE;

        set @strTimestampFrom := UNIX_TIMESTAMP(@currDateFrom);
        set @strTimestampTo := UNIX_TIMESTAMP(dateTo);
        set @query2 = concat('insert into tmptable SELECT '', DATE_FORMAT(@currDateTo,'%Y-%m-%d'), '', SUM(`transaction`.`amount`), `category`.`name` as `category` FROM `transaction` INNER JOIN `category` on `category`.`id` = `transaction`.`category` WHERE `transaction`.`amount` > 0 and `transaction`.`user` = ', userid, ' and UNIX_TIMESTAMP(`transaction`.`date`) BETWEEN ', @strTimestampFrom ,' AND ', @strTimestampTo , ' GROUP BY `category`;');
        PREPARE tmpQ from @query2;
        EXECUTE tmpQ;
    END;
  END IF;
 END LOOP;


    -- BEGIN
    --     DECLARE curs1 CURSOR FOR SELECT date1, category, balance FROM `tmptable` order by `category`;
    --     DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    --     SET @json = '{"data":[';

    --     OPEN curs1;

    --     my_loop:
    --      LOOP
    --       FETCH FROM curs1 INTO v_date, v_category, v_balance;

    --       IF done THEN 
    --         LEAVE my_loop;
    --       ELSE
    --         SET @json = CONCAT(@json, '{"date": "', v_date, '", "category": "', v_category, '", "balance": "', v_balance, '"},');
    --       END IF;
    --      END LOOP;

    --     SET jSON = CONCAT(TRIM(TRAILING ',' FROM @json), ']}');

    --     SELECT jSON;

    --     DROP TABLE IF EXISTS `tmptable`;
    -- END
END