<?php
    include_once "./classes/enums.php";
    include_once "./classes/user.php";
    include_once "./classes/sqlcontroller.php";
    include_once "./classes/consettings.php";
    include_once "./classes/settingscontroller.php";
    include_once "./classes/accountcontroller.php";
    include_once "./classes/categorycontroller.php";
    include_once "./classes/transactioncontroller.php";
    include_once "./classes/usercontroller.php";

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header("Access-Control-Allow-Credentials: true");
    header("Content-Type: text/html; charset=utf-8");

    $main_controller = new MainController();

    if ($_SERVER['REQUEST_METHOD']==='POST') {
        $main_controller->doPost();
    } else if ($_SERVER['REQUEST_METHOD']==='GET') {
        $main_controller->doGet();
    } else {
        die ("Only GET and POST methods are allowed.");
    }

    class MainController {
        private $connect_info;
        private $sql_controller;
        private $setting_controller;
        private $account_controller;
        private $caegory_controller;
        private $user_controller;
        private $cam_controller;
        private $transaction_controller;

        public function __construct() {
            $this->connect_info = new ConSettings("localhost", "3306", "root", "123456", "buh");
            $this->sql_controller = new SqlController($this->connect_info); 
            $this->setting_controller = new SettingController($this->sql_controller);
            $this->user_controller = new UserController($this->sql_controller);
            $this->account_controller = new AccountController($this->sql_controller);
            $this->category_controller = new CategoryController($this->sql_controller);
            $this->transaction_controller = new TransactionController($this->sql_controller);
        }

        private function auth($login, $pwd) {
            return $this->user_controller->authenticate(new User($login, $pwd)) ;
        }

        private function signIn($login, $pwd, $email) {
            $user = new User($login, $pwd);
            $user->email = $email;
            return $this->user_controller->signIn($user) ;
        }

        public function doGet() {
            if(isset($_GET['confirm'])) {
                if (1 == $this->user_controller->confirm($_GET['confirm'])) { 
                    header('Location: https://www.litemoney.online');
                }
            } 
            die ("Something strange happened... Please, try again!");
        }

        private function checkInput($command) {
            $result = false;
            if (isset($command['user_id'])) {
                $result = is_numeric($command['user_id']) && (0 != (int)$command['user_id']);
            }
            if (isset($command['from'])) {
                $result &= is_numeric($command['from']) && (0 != (int)$command['from']);
            }
            if (isset($command['to'])) {
                $result &= is_numeric($command['to']) && (0 != (int)$command['to']);
            }
            if (isset($command['itype'])) {
                $result &= is_numeric($command['itype']);
            }
            if (isset($command['account'])) {
                $result &= is_numeric($command['account']);
            }
            if (isset($command['category'])) {
                $result &= is_numeric($command['category']);
            }
            if (isset($command['reptype'])) {
                $result &=is_numeric($command['reptype']);
            }
            if (isset($command['cat_type']) && null != $command['cat_type']) {
                $result &= is_numeric($command['cat_type']);
            }
            if (isset($command['trans_id'])) {
                $result &= is_numeric($command['trans_id']);
            }

            return $result;
        }

        public function doPost() {
            //Try to get command from POST
            if(isset($_POST['command'])) {
                //default result
                $result = Array('data' => '0');
                $commJson = $_POST['command'];
                $comm = json_decode($commJson, true);

                switch($comm['type']) {
                    case Command::Login:
                        $login  = $comm['login'];
                        $pwd    = $comm['password'];
                        $result['data'] = $this->auth($login, $pwd);
                    break;

                    case Command::SignIn:
                        $result['data'] = $this->signIn($comm['login'], $comm['password'], $comm['email']); 
                    break;

                    case Command::GetAllSettings:
                        $result['data'] = $this->setting_controller->GetAllSettings($comm['user_id']);
                    break;

                    case Command::SetAllSettings:
                        $result['data'] = $this->setting_controller->SetAllSettings($comm['settings']);
                    break;

                    case Command::GetAccountList:
                        $result['data'] = $this->account_controller->GetAccountList($comm['user_id']);
                    break;

                    case Command::GetCurrencyList:
                        $result['data'] = $this->account_controller->GetCurrencyList($comm['user_id']);
                    break;

                    case Command::SaveAccount:
                        $result['data'] = $this->account_controller->SaveAccount($comm['user_id'], $comm['account']);
                    break;

                    case Command::DeleteAccount:
                        $result['data'] = $this->account_controller->DeleteAccount($comm['user_id'], $comm['account']);
                    break;

                    case Command::CreateAccount:
                        $result['data'] = $this->account_controller->CreateAccount($comm['user_id'], $comm['account']);
                    break;

                    case Command::NotifyMe:
                        $result['data'] = $this->user_controller->NotifyMe($comm['email']);
                    break;

                    case Command::GetCategoryList:
                        if(isset($comm['cat_type']) && (0 != $comm['cat_type']) ) {
                            $result['data'] = $this->category_controller->GetCategoryListByType($comm['user_id'], $comm['cat_type']);
                        } else {
                            $result['data'] = $this->category_controller->GetCategoryList($comm['user_id']);
                        }
                    break;

                    case Command::GetTypeList:
                        $result['data'] = $this->category_controller->GetTypeList($comm['user_id']);
                    break;

                    case Command::SaveCategory:
                        $result['data'] = $this->category_controller->SaveCategory($comm['user_id'], $comm['category']);
                    break;

                    case Command::DeleteCategory:
                        $result['data'] = $this->category_controller->DeleteCategory($comm['user_id'], $comm['category']);
                    break;

                    case Command::CreateCategory:
                        $result['data'] = $this->category_controller->CreateCategory($comm['user_id'], $comm['category']);
                    break;

                    case Command::ConfirmTransaction:
                        $result['data'] = $this->transaction_controller->ConfirmTransaction($comm['user_id'], $comm['transaction']);
                    break;

                    case Command::EditTransaction:
                        $result['data'] = $this->transaction_controller->GetTransaction($comm['user_id'], $comm['trans_id']);
                    break;

                    case Command::DeleteTransaction:
                        $result['data'] = $this->transaction_controller->DeleteTransaction($comm['user_id'], $comm['transaction']);
                    break;

                    case Command::GetReport:
                        if(!$this->checkInput($comm)) {
                            die ("Bad data");
                        }
                        switch ($comm['reptype']) {
                            case 1:
                                $result['data'] = $this->transaction_controller->GetBasicReport($comm['user_id'], $comm['from'], $comm['to'], $comm['itype']);
                            break;

                            case 2:
                                $result['data'] = $this->transaction_controller->GetAccountReport($comm['user_id'], $comm['from'], $comm['to'], (isset($comm['account']) ? $comm['account'] : 0), $comm['itype']);
                            break;

                            case 3:
                                $result['data'] = $this->transaction_controller->GetCategoryReport($comm['user_id'], $comm['from'], $comm['to'], (isset($comm['account']) ? $comm['account'] : 0), $comm['category']);
                            break;

                            case 4:
                                $result['data'] = $this->transaction_controller->GetAccountPiechart($comm['user_id'], $comm['from'], $comm['to'], (isset($comm['account']) ? $comm['account'] : 0), $comm['itype']);
                            break;

                            case 6:
                                $result['data'] = $this->transaction_controller->GetBalanceChart($comm['user_id'], $comm['from'], $comm['to'], $comm['itype']);
                            break;

                            case 7:
                                $result['data'] = $this->transaction_controller->GetBalanceChartDetail($comm['user_id'], $comm['from'], $comm['to'], $comm['itype']);
                            break;

                            case 8:
                            case 9:
                                $result['data'] = $this->transaction_controller->GetIncomeChart($comm['user_id'], $comm['from'], $comm['to'], $comm['itype'], $comm['reptype']);
                            break;

                            case 10:
                            case 11:
                                $result['data'] = $this->transaction_controller->GetTotalIncomeChart($comm['user_id'], $comm['from'], $comm['to'], $comm['itype'], $comm['reptype']);
                            break;

                            default:
                            break;
                        }
                    break;

                    default:
                    break;
                }
            
                die (json_encode($result, JSON_UNESCAPED_UNICODE ));
            
            } else {
                die ("Command not found");
            }
        }
    }
?>